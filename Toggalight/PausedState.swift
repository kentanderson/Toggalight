//
//  PausedState.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation

class PausedState : BaseSceneState {
    override var className:String { return "PausedState" }

    
    override func enterState(_ scene:GameScene) {
        super.enterState(scene)
        setButtons(startOver:true, resume:true, howToPlay:true, calibrate:true, highScore:true)
        scene.frozen = true
        scene.enableIdleTimer()
    }
    
    
    override func onStartOverButton() {
        scene!.shouldReset = true
        scene!.transitionToState(super.scene!.runningState)
    }
    
    override func onResumeButton() {
        scene!.shouldReset = false
        scene!.transitionToState(super.scene!.runningState)
    }
}
