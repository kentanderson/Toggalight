//
//  MotionManager.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import CoreMotion

struct AdjustedAttitude {
    var roll : Double
    var pitch : Double
    var yaw : Double
}

protocol MotionManager {
    var rawMotionManager: CMMotionManager { get }
    var currentAttitude: AdjustedAttitude { get }
    var currentRawAttitude: CMAttitude { get }
    
    func calibrate()
    func startUpdates()
    func stopUpdates()
}

class NullMotionManager: MotionManager {
    var rawMotionManager : CMMotionManager {
        return CMMotionManager()
    }
    
    var currentAttitude : AdjustedAttitude {
        return AdjustedAttitude(roll:0, pitch:0, yaw:0)
    }
    
    var currentRawAttitude : CMAttitude {
        return CMAttitude()
    }
    
    init(){
    }
    
    func calibrate(){
    }
    
    func startUpdates() {
    }
    
    func stopUpdates() {
    }
}

class RealMotionManager : MotionManager {

    fileprivate let _rawMotionManager = CMMotionManager()
    fileprivate var referencePitch = 0.0
    fileprivate var referenceRoll = 0.0
    fileprivate var referenceYaw = 0.0

    
    var rawMotionManager : CMMotionManager {
        return _rawMotionManager
    }
    
    var currentAttitude : AdjustedAttitude {
        var att = AdjustedAttitude(roll:0, pitch:0, yaw:0)
        let cmAtt = currentRawAttitude
        att.pitch = cmAtt.pitch - referencePitch
        att.roll = cmAtt.roll - referenceRoll
        att.yaw = cmAtt.yaw - referenceYaw
        return att
    }
    
    var currentRawAttitude : CMAttitude {
        return _rawMotionManager.deviceMotion!.attitude
    }
    
    init(){
    }
    
    func calibrate(){
        let cmAtt = currentRawAttitude
        let adj = 0.85
        referencePitch = cmAtt.pitch * adj
        referenceRoll = cmAtt.roll * adj
        referenceYaw = cmAtt.yaw * adj
    }
    
    func startUpdates() {
        _rawMotionManager.deviceMotionUpdateInterval = 1.0/60.0
        _rawMotionManager.startDeviceMotionUpdates()
    }
    
    func stopUpdates() {
        _rawMotionManager.stopDeviceMotionUpdates()
    }
}

let globalMotionManager:MotionManager = CMMotionManager().isDeviceMotionAvailable ? RealMotionManager() : NullMotionManager()
