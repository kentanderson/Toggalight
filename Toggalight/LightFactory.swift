//
//  LightFactory.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import UIKit

class LightFactory {
    
    init(){
        var timeVal: timeval = timeval(tv_sec: 0, tv_usec: 0)
        gettimeofday(&timeVal, nil)
        srandom(UInt32(timeVal.tv_usec))
    }
    
    func getLights(_ count: Int, widthPct: Float ) -> [Light] {
        var lights: [Light] = []
        for _ in 0..<count {
            let light = Light()
            var overlapped = false
            
            repeat {
                light.position = CGPoint(
                    x:CGFloat(arc4random() % 100) / CGFloat(100.0),
                    y:CGFloat(arc4random() % 100) / CGFloat(100.0))
                
                for existing in lights {
                    // Yes, this is N^2. It's not a big deal.
                    let existingRect = CGRect(x: existing.position.x - CGFloat(widthPct/2),
                        y: existing.position.y - CGFloat(widthPct/2),
                        width: CGFloat(widthPct), height: CGFloat(widthPct))
                    
                    let newRect = CGRect(x: light.position.x - CGFloat(widthPct/2),
                        y: light.position.y - CGFloat(widthPct/2),
                        width: CGFloat(widthPct), height: CGFloat(widthPct))
                    
                    overlapped = newRect.intersects(existingRect)
                    if overlapped {
                        break;
                    }
                }
            
            } while overlapped == true
            lights.append(light)
        }
        
        return lights
    }
}
