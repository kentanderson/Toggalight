//
//  SceneState.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import SpriteKit

protocol SceneState {
    var className: String { get }
    
    func enterState (_ scene:GameScene)
    func exitState ()

    func update (_ currentTime:CFTimeInterval)
    func lightTouched (_ light:Light)
    func onStartButton ()
    func onStartOverButton ()
    func onPauseButton ()
    func onResumeButton ()
}
