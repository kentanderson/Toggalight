//
//  Light.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import SpriteKit

let LightOn = SKTexture( imageNamed:"LightOn" )
let LightOff = SKTexture( imageNamed:"LightOff" )


class Light {
    
    var node : SKSpriteNode?
    var position : CGPoint
    
    init() {
        self.node = nil
        self.position = CGPoint(x: 0,y: 0)
    }
    
    init(node:SKSpriteNode, position:CGPoint) {
        self.node = node
        self.position = position
    }
    
    var isOn : Bool {
        get {
            if node!.texture == LightOn {
                return true
            }
            return false
        }
        
        set {
            if newValue == true {
                node!.texture = LightOn
            } else {
                node!.texture = LightOff
            }
        }
    }

}