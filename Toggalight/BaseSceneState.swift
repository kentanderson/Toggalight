//
//  BaseSceneState.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import SpriteKit

class BaseSceneState : SceneState {
    
    var className : String { return "BaseSceneState" }
    
    
    fileprivate var _scene : GameScene?
    
    
    init(){
    }
    
    var scene : GameScene? {
        get { return _scene }
        set {
            _scene = newValue
            _scene!.sceneState = self
        }
    }
    
    func enterState(_ scene: GameScene) {
        self.scene = scene
    }
    
    func exitState() {
        _scene = nil
    }
    
    func update(_ currentTime: CFTimeInterval) {}
    func lightTouched(_ light: Light) {}
    func onStartButton() {}
    func onPauseButton() {}
    func onStartOverButton() {}
    func onResumeButton() {}
    
    func setButtons(
        _ start:Bool = false,
        pause:Bool = false,
        startOver:Bool = false,
        resume:Bool = false,
        howToPlay:Bool = false,
        calibrate:Bool = false,
        highScore:Bool = false
        ){
            scene!.startButton!.isHidden = !start
            scene!.pauseButton!.isHidden = !pause
            scene!.startOverButton!.isHidden = !startOver
            scene!.resumeButton!.isHidden = !resume
            scene!.howToPlayButton!.isHidden = !howToPlay
            scene!.calibrateButton!.isHidden = !calibrate
            scene!.highScoreLabel!.isHidden = !highScore
        
    }
    
}
