//
//  Score.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation

class Score {
    var stars = 0
    var totalStars = 0
    
    init(){}
    init(copy:Score) {
        stars = copy.stars
        totalStars = copy.totalStars
    }
    
    func greaterThan(_ other:Score) -> Bool {
        if totalStars > other.totalStars {
            return true
        } else if (totalStars == other.totalStars) && (stars > other.stars) {
            return true
        }
        return false
    }
    
    func copy() -> Score {
        return Score(copy: self)
    }
    
}
