//
//  GameScene.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import SpriteKit

let BALL_CATEGORY : UInt32 = 1
let LIGHT_CATEGORY : UInt32 = 1 << 1
let BorderWidth : Float = 30.0

let GRAY_COLOR = SKColor(red: 64.0/255.0, green: 24.0/255, blue: 64.0/255, alpha: 1.0)
let LIGHTGRAY_COLOR = SKColor(red: 192.0/255, green: 192.0/255, blue: 192.0/255, alpha: 1.0)
let RED_COLOR = SKColor(red: 0.4, green: 0.1, blue: 0.1, alpha: 1.0)
let YELLOW_COLOR = SKColor(red: 1, green: 1, blue: 0, alpha: 1)

class GameScene : SKScene, SKPhysicsContactDelegate {
    
    let lightFactory : LightFactory = LightFactory()
    let defaultLabelColor : SKColor = GRAY_COLOR
    let iPhoneScreen:Bool = UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone ? true : false
    let iPhoneScale = 0.7
    
    fileprivate var _clock: Int = 0
    
    var clock: Int { get { return _clock } set { setClockValue(newValue) } }
    var lightCount : Int = 1
    var currentScore : Score = Score()
    var highScore : Score = Score()

    var lights : [Light] = []
    
    var clockLabel : SKLabelNode = SKLabelNode(fontNamed: "Futura")
    var levelLabel : SKLabelNode = SKLabelNode(fontNamed: "Futura-CondensedExtraBold")
    var scoreLabel : SKLabelNode = SKLabelNode(fontNamed: "Futura-CondensedExtraBold")
    var ball : SKSpriteNode = SKSpriteNode(imageNamed: "Ball")
    
    weak var startButton : UIButton?
    weak var pauseButton : UIButton?
    weak var startOverButton : UIButton?
    weak var resumeButton : UIButton?
    weak var calibrateButton : UIButton?
    weak var howToPlayButton : UIButton?
    weak var highScoreLabel : UILabel?
    
    let preStartState : SceneState = PreStartState()
    let runningState : SceneState = RunningState()
    let pausedState : SceneState = PausedState()
    let gameOverState : SceneState = GameOverState()
    
    var sceneState : SceneState = PreStartState()
    
    var shouldReset : Bool = true
    var layoutInProgress : Bool = false
    
    var calibrated : Bool = false

    var frozen: Bool {
        get { return !ball.physicsBody!.isDynamic }
        set {
            let tmpBody : SKPhysicsBody = ball.physicsBody!
            tmpBody.isDynamic = !newValue
            ball.physicsBody = tmpBody
        }
    }
    
    required init(coder aCoder : NSCoder){
        fatalError("init(coder:) has not been implemented")
    }

    override init(size: CGSize) {
        super.init(size:size)
        initializePhysics()
    }
    
    private func initializePhysics() {
        self.physicsWorld.gravity = CGVector(dx: 0,dy: -1)
        self.physicsWorld.speed = 3.5
        self.physicsWorld.contactDelegate = self
        
        self.backgroundColor = SKColor(red: 0, green: 0, blue: 0, alpha: 1.0)
        
        self.physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        self.physicsBody!.isDynamic = true
        self.physicsBody!.affectedByGravity = false
        self.physicsBody!.friction = 0.1
        
        if let starFieldFile = Bundle.main.url(forResource: "Starfield",
                                               withExtension: "sks") {
            do {
                let data = try Data(contentsOf: starFieldFile)
                if let starField = try NSKeyedUnarchiver.unarchivedObject(ofClass: SKEmitterNode.self, from: data) {
                    starField.position = CGPoint(x: self.frame.midX,
                                                 y: self.frame.midY)
                    self.addChild(starField)
                }
            } catch {}
        }
        
        levelLabel.fontColor = defaultLabelColor
        levelLabel.fontSize = iPhoneScreen ? 48 : 96
        let locX:CGFloat = iPhoneScreen ? 0.87 : 0.90
        let locY:CGFloat = iPhoneScreen ? 0.15 : 0.90
        levelLabel.position = CGPoint(x: self.frame.width * locX, y: self.frame.height * locY)
        self.addChild(levelLabel)
        
        clockLabel.fontColor = levelLabel.fontColor
        clockLabel.fontSize = iPhoneScreen ? 24 : 36
        clockLabel.position = CGPoint(x: levelLabel.position.x, y: levelLabel.position.y - 15)
        clockLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        clockLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        clockLabel.text = "0:05"
        clockLabel.text = ""
        self.addChild(clockLabel)
        
        scoreLabel.fontColor = levelLabel.fontColor
        scoreLabel.fontSize = iPhoneScreen ? 18 : 24
        scoreLabel.position = CGPoint(x: levelLabel.position.x, y: clockLabel.position.y - 40)
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
        scoreLabel.text = ""
        self.addChild(scoreLabel)
        
        ball.xScale = iPhoneScreen ? CGFloat(iPhoneScale) : 1.0
        ball.yScale = ball.xScale
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.size.width*0.8/2)
        ball.physicsBody!.isDynamic = true
        ball.physicsBody!.mass = 0.01
        ball.physicsBody!.friction = 1.0
        ball.physicsBody!.linearDamping = 0.25
        ball.physicsBody!.categoryBitMask = BALL_CATEGORY
        ball.physicsBody!.collisionBitMask = BALL_CATEGORY
        ball.physicsBody!.contactTestBitMask = BALL_CATEGORY | LIGHT_CATEGORY
        ball.userData = ["type":"ball"]
        ball.position = CGPoint(x: self.frame.midX, y: self.frame.midY)
        ball.zPosition = 100
        self.addChild(ball)
    }

    override func didMove(to view: SKView) {
        self.readHighScore()
        globalMotionManager.startUpdates()
        sceneState = preStartState
        sceneState.enterState(self)
    }
    
    func transitionToState(_ state : SceneState) {
        sceneState.exitState()
        sceneState = state
        sceneState.enterState(self)
    }
    
    func onStartButton(){
        sceneState.onStartButton()
    }
    
    func onStartOverButton(){
        sceneState.onStartOverButton()
    }
    
    func onPauseButton(){
        sceneState.onPauseButton()
    }
    
    func onResumeButton(){
        sceneState.onResumeButton()
    }
    
    func onCalibrateButton() {
        let hLaser = SKSpriteNode(imageNamed: "Laser")
        hLaser.position = CGPoint(x: self.frame.midX, y: self.frame.size.height + hLaser.size.height/2)
        self.addChild(hLaser)
        
        delay(0.1){
            self.calibrateButton!.isEnabled = false
            self.calibrateButton!.titleLabel?.text = "Hold..."
        }
        
        delay(0.2){
            hLaser.run(SKAction.sequence([
                SKAction.move(by: CGVector(dx:0, dy:-(self.frame.size.height + hLaser.size.height)), duration:1),
                SKAction.run(){
                    hLaser.removeFromParent()
                    }
                ]))
        }
        
        // half way
        delay(0.5){
            globalMotionManager.calibrate()
        }
        
        delay(1.3){
            self.calibrateButton!.isEnabled = true;
        }
    }
    
    func setGameOverPhysics() {
        for light in lights {
            if light.isOn == false {
                light.node!.physicsBody!.isDynamic = true
                light.node!.physicsBody!.affectedByGravity = true
                light.node!.physicsBody!.mass = CGFloat(arc4random() % 30)
            }
        }

        ball.physicsBody!.collisionBitMask = BALL_CATEGORY | LIGHT_CATEGORY
        ball.physicsBody!.contactTestBitMask = 0
    }
    
    func setGameRunningPhysics() {
        physicsWorld.gravity = CGVector(dx: 0, dy: 0);
        ball.physicsBody!.collisionBitMask = BALL_CATEGORY
        ball.physicsBody!.contactTestBitMask = BALL_CATEGORY | LIGHT_CATEGORY
    }
    
    func enableIdleTimer() {
        delay(10.0){
            if (self.sceneState.className == self.pausedState.className){
                UIApplication.shared.isIdleTimerDisabled = true
            }
        }
    }
    
    func checkHighScore() {
        if currentScore.greaterThan(highScore) {
            highScore = currentScore.copy()
            saveHighScore(true)
        } else {
            updateStandingHighScoreLabel(false)
        }
    }
    
    func updateScore() {
        scoreLabel.text = "\(currentScore.stars) / \(currentScore.totalStars)"
        scoreLabel.fontColor = currentScore.greaterThan(highScore) ? YELLOW_COLOR : defaultLabelColor
    }

    override func update(_ currentTime: TimeInterval) {
        sceneState.update(currentTime)
    }
    
    func setClockValue(_ value: Int) {
        _clock = max(0, value)
        clockLabel.text = String(format:"%d:%02d", (clock / 60), (clock % 60))
        if _clock > 0 && _clock <= 3 {
            clockLabel.fontColor = RED_COLOR
            levelLabel.fontColor = clockLabel.fontColor
            
            let throb: SKLabelNode = clockLabel.copy() as! SKLabelNode
            throb.position = clockLabel.position
            throb.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top
            throb.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center
            self.addChild(throb)
            throb.run(SKAction.sequence([
                SKAction.group([
                    SKAction.scale(to: 3, duration: 0.5),
                    SKAction.fadeOut(withDuration: 0.5),
                    SKAction.move(by: CGVector(dx:0,dy:30), duration:0.5)
                ]),
                SKAction.run({
                    throb.removeFromParent()
                })
            ]))
        } else {
            clockLabel.fontColor = defaultLabelColor
            levelLabel.fontColor = defaultLabelColor
        }
    }
    
    func findLight(_ node:SKNode) -> Light? {
        for light in lights {
            if light.node == node {
                return light
            }
        }
        return nil
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        for node in [contact.bodyA.node, contact.bodyB.node] {
            if let nodeType:String = node!.userData?.object(forKey: "type") as? String {
                if nodeType == "light" {
                    let light = findLight(node!)
                    if light == nil {
                        continue
                    }
                    
                    sceneState.lightTouched(light!)
                    break
                }
            }
        }
    }
   
    func repositionLights() {
        if sceneState.className == gameOverState.className {
            return
        }
        
        for light in lights {
            let xPos : CGFloat = CGFloat(BorderWidth + (Float(frame.size.width) - (2.0*BorderWidth)) * Float(light.position.x))
            let yPos : CGFloat = CGFloat(BorderWidth + (Float(frame.size.height) - (2.0*BorderWidth)) * Float(light.position.y))
            light.node!.position = CGPoint(x:xPos, y:yPos)
        }
    }
    
    override func didChangeSize(_ oldSize: CGSize) {
        physicsBody = SKPhysicsBody(edgeLoopFrom: self.frame)
        physicsBody!.isDynamic = true
        physicsBody!.affectedByGravity = false
        physicsBody!.friction = 0.2
        repositionLights()
        
        // Fail-safe.  Sometimes the ball can get forced out of bounds
        // if an ad appears and it's pressed up against the top wall.
        // Just end the game if that happens.
        if (!self.frame.contains(ball.position)) {
            transitionToState(gameOverState)
        }
    }
    
    func layoutNewField() {
        for light in lights {
            light.node?.removeFromParent()
            light.node = nil
        }
        
        lightCount += 1
        let refNode = SKSpriteNode(imageNamed: "LightOff")
        refNode.xScale = iPhoneScreen ? 0.80 * CGFloat(iPhoneScale) : 0.80
        refNode.yScale = refNode.xScale
        
        let newlights = lightFactory.getLights(lightCount,
            widthPct: Float((refNode.frame.size.width / self.frame.size.width)))
        
        for light in newlights {
            let lightNode : SKSpriteNode = SKSpriteNode(imageNamed: "LightOff")
            lightNode.xScale = iPhoneScreen ? 0.80 * CGFloat(iPhoneScale) : 0.80
            lightNode.yScale = lightNode.xScale
            lightNode.physicsBody = SKPhysicsBody(circleOfRadius: (lightNode.size.width*0.90)/2)
            lightNode.physicsBody!.isDynamic = false
            lightNode.physicsBody!.mass = 1.0
            lightNode.physicsBody!.friction = 1.0
            lightNode.physicsBody!.affectedByGravity = false
            lightNode.physicsBody!.categoryBitMask = LIGHT_CATEGORY
            lightNode.physicsBody!.collisionBitMask = LIGHT_CATEGORY
            lightNode.physicsBody!.contactTestBitMask = BALL_CATEGORY | LIGHT_CATEGORY
            lightNode.userData = ["type":"light"]
            lightNode.zPosition = 10
            addChild(lightNode)
            
            light.node = lightNode
            light.isOn = false
        }
        
        lights = newlights
        repositionLights()
        levelLabel.text = String(format:"%d", lightCount)
        
        currentScore.totalStars = lightCount
        currentScore.stars = 0
        updateScore()
        clock += 5 + (3 * (lightCount/5))
    }
    
    let STARS_HIGH_SCORE = "HS:Stars"
    let TOTAL_HIGH_SCORE = "HS:Total"

    func readHighScore() {
        highScore = Score()
        let stars = UserDefaults.standard.integer(forKey: STARS_HIGH_SCORE)
        let total = UserDefaults.standard.integer(forKey: TOTAL_HIGH_SCORE)
        if stars > 255 || total > 255 {
            return
        }
        highScore.stars = stars
        highScore.totalStars = total
        updateStandingHighScoreLabel(false)
    }

    func saveHighScore(_ isNew:Bool){
        UserDefaults.standard.set(highScore.stars, forKey: STARS_HIGH_SCORE)
        UserDefaults.standard.set(highScore.totalStars, forKey: TOTAL_HIGH_SCORE)
        updateStandingHighScoreLabel(isNew)
    }
    
    func updateStandingHighScoreLabel(_ isNew: Bool) {
        if highScore.totalStars == 0 && highScore.stars == 0 {
            // empty
            highScoreLabel!.text = ""
            return
        }
        var newText = ""
        if isNew { newText = "New " }
        highScoreLabel!.text = "\(newText)High Score   \(highScore.totalStars)  \(highScore.stars)/\(highScore.totalStars)"
        highScoreLabel!.textColor = isNew ? YELLOW_COLOR : LIGHTGRAY_COLOR
    }
    
    #if SIMDEBUG
    /* Let's me test stuff in the simulator. */
    func setBallPosition(touch: UITouch?) {
        let point = touch!.location(in: self)
        ball.position = point
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        setBallPosition(touch: touches.first as UITouch?)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        setBallPosition(touch: touches.first as UITouch?)
    }
    #endif
}
