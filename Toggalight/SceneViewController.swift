//
//  SceneViewController.swift
//  Toggalight
//
//  Created by Kent Anderson on 12/29/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class SceneViewController : UIViewController {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
    }
    
    fileprivate var _scene : GameScene?
    fileprivate var _originalFrame : CGRect?
    
    @IBOutlet weak var startButton : UIButton?
    @IBOutlet weak var pauseButton : UIButton?
    @IBOutlet weak var resumeButton : UIButton?
    @IBOutlet weak var startOverButton : UIButton?
    @IBOutlet weak var howToPlayButton : UIButton?
    @IBOutlet weak var calibrateButton : UIButton?
    @IBOutlet weak var highScoreLabel : UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatButtons()
        initializeGameScene()
    }
    
    private func formatButtons() {
        [startButton, pauseButton, resumeButton,
         startOverButton, howToPlayButton, calibrateButton].forEach { button in
            button!.layer.cornerRadius = 10
            button!.clipsToBounds = true
         }
    }
    
    private func initializeGameScene() {
        // Configure the view.
        if let skView = self.view as? SKView {
            skView.showsFPS = false;
            skView.showsNodeCount = false;
            
            _scene = GameScene(size: skView.bounds.size)
            
            if let scene = _scene {
                _originalFrame = scene.frame
                scene.scaleMode = .resizeFill
                
                scene.startButton = startButton!
                scene.pauseButton = pauseButton!
                scene.startOverButton = startOverButton!
                scene.resumeButton = resumeButton!
                scene.howToPlayButton = howToPlayButton!
                scene.calibrateButton = calibrateButton!
                scene.highScoreLabel = highScoreLabel!
                
                // Present the scene.
                skView.presentScene(_scene)
            }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        _scene!.onPauseButton()
        //_scene!.view.paused = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override var shouldAutorotate : Bool {
        return false
    }
    
//    override func supportedInterfaceOrientations() -> Int {
//        return UIInterfaceOrientationMask.Portrait
//    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func onStartButton(_ id:AnyObject) {
        _scene!.onStartButton()
    }
   
    @IBAction func onPauseButton(_ id:AnyObject) {
        _scene!.onPauseButton()
    }
    
    @IBAction func onResumeButton(_ id:AnyObject) {
        _scene!.onResumeButton()
    }
    
    @IBAction func onStartOverButton(_ id:AnyObject) {
        _scene!.onStartOverButton()
    }
    
    @IBAction func onCalibrateButton(_ id:AnyObject) {
        _scene!.onCalibrateButton()
    }

}

