//
//  PreStartState.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation

class PreStartState : BaseSceneState {
    override var className:String { return "PreStartState" }
    
    override init() {}
    
    override func enterState(_ scene:GameScene) {
        super.enterState(scene)
        setButtons(true, howToPlay:true, calibrate:true, highScore:true)
        scene.frozen = true
    }
    
    
    override func onStartButton() {
        scene!.transitionToState(scene!.runningState)
    }
    
    
}
