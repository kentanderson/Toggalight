//
//  HelpViewController.swift
//  Toggalight
//
//  Created by Kent Anderson on 12/29/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import UIKit


class HelpViewControler : UIViewController {

    required init?(coder aDecoder: NSCoder) {
        //fatalError("init(coder:) has not been implemented")
        super.init(coder:aDecoder)
    }
    
    @IBOutlet weak var goBackButton: UIButton?
    
    override func viewDidLoad(){
        super.viewDidLoad()
        goBackButton!.layer.cornerRadius = 10
        goBackButton!.clipsToBounds = true
    }
    
    @IBAction func onGoBackButton(_ sender:AnyObject){
        self.dismiss(animated: true, completion:nil)
    }
}
