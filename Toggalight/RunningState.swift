//
//  RunningState.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import UIKit

class RunningState : BaseSceneState {
    override var className:String { return "RunningState" }
    
    fileprivate var startTime : CFTimeInterval = 0
    
    override func enterState(_ scene : GameScene){
        super.enterState(scene)
        //UIApplication.sharedApplication().setIdleTimerDisabled = true
        
        setButtons(pause:true)
        
        if let btn = scene.pauseButton { btn.alpha = 0.6 }
        
        if (scene.calibrated == false)
        {
            globalMotionManager.calibrate()
            scene.calibrated = true
        }
        
        scene.setGameRunningPhysics()
        
        if (scene.shouldReset){
            scene.lightCount = 0
            scene.clock = 0
            scene.layoutInProgress = true
            scene.layoutNewField()
            scene.layoutInProgress = false
            scene.ball.position = CGPoint(x: scene.frame.midX, y: scene.frame.midY)
            scene.ball.physicsBody!.velocity = CGVector(dx: 0,dy: 0)
        }
        
        scene.frozen = false
        startTime = 0
    }
    
    override func update(_ currentTime : CFTimeInterval){
        if startTime == 0 {
            startTime = currentTime
        }

        let attitude = globalMotionManager.currentAttitude
        scene!.physicsWorld.gravity = CGVector(dx:2.0 * attitude.roll, dy:-2.0*attitude.pitch)
        
        if currentTime - startTime > 1 {
            scene!.clock -= 1
            startTime = currentTime
            if scene!.clock <= 0 && scene!.layoutInProgress == false {
                scene!.transitionToState(scene!.gameOverState)
            }
        }
    }
    
    override func onPauseButton() {
        scene!.transitionToState(scene!.pausedState)
    }
    
    
    override func lightTouched(_ light : Light) {
        if scene!.layoutInProgress {
            return
        }

        light.isOn = !light.isOn
        scene!.currentScore.stars += light.isOn ? 1 : -1
        scene!.updateScore()

        var allOn = true
        for lite in scene!.lights {
            if !lite.isOn {
                allOn = false
                break
            }
        }
        
        if allOn {
            scene!.layoutInProgress = true
            delay(0.5){
                    self.scene!.layoutNewField()
                    self.scene!.layoutInProgress = false
                }
        }
    }
    
}
