//
//  GameOverState.swift
//  Toggalight
//
//  Created by Kent Anderson on 10/14/14.
//  Copyright (c) 2014 Kornerstoane Creativity, LLC. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverState : BaseSceneState {
    override var className: String { return "GameOverState" }
    
    override func enterState(_ scene:GameScene) {
        super.enterState(scene)
        setButtons(startOver:true, howToPlay:true, calibrate:true, highScore:true)
        scene.checkHighScore()
        scene.setGameOverPhysics()
    }
    
    override func exitState() { super.scene!.frozen = false }
    
    override func update(_ currentTime:CFTimeInterval){
        let attitude = globalMotionManager.currentAttitude
        scene!.physicsWorld.gravity = CGVector(dx:CGFloat(2.0*attitude.roll), dy:CGFloat(-2.0*attitude.pitch))
    }
    
    override func onStartOverButton() {
        DispatchQueue.main.async{
            self.scene!.shouldReset = true
            self.scene!.transitionToState(self.scene!.runningState)
        }
    }
}
